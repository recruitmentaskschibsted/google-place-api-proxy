let Handler = require('./Handler.js');

module.exports = class ListHandler extends Handler{

  constructor(req, res){
    super(req, res)
  }
  googleRequest(){
      let params = {
        language: 'pl',
        location: [50.061702102439874, 19.937287288427797],
        radius: 2000,
        type: 'bar'};

      if(this.req.query.filter){
        params.name = this.req.query.filter;
      }
      if(this.req.query.pagetoken){
        params.pagetoken = this.req.query.pagetoken;
      }
      
      this.req.gclient.placesNearby(
          params,
          (err, response) => {

            if (err) {
              this.emitter.emit('error', err, 501);
              return;
            }
            this.saveCache(response.json);
            this.emitter.emit('dataready', response.json);
      });
  }
  getCacheKey(){

    let pagetoken = this.req.query.pagetoken || '',
      filter = this.req.query.filter || '',
      cacheKey = 'list-'+pagetoken+'-'+filter;
      console.log('cacheKey', cacheKey)
      return cacheKey;
  };
};
