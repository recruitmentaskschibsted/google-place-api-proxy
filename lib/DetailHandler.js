let ee = require('event-emitter'),
  Handler = require('./Handler.js');

module.exports = class DetailHandler extends Handler {
  constructor(req, res){
    super(req, res);
  }

  googleRequest(){
      let params = {
        language: 'pl',
        placeid:  this.req.params.id};

      this.req.gclient.place(
          params,
          (err, response) => {
            if (err) {
              this.emitter.emit('error', err, 501);
              return;
            }
            this.saveCache(response.json);
            this.emitter.emit('dataready', response.json);
          });
  }

  getCacheKey(){
    return 'detail-'+this.req.params.id;
  }
};
