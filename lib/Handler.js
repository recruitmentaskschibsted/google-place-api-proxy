'use strict';
let ee = require('event-emitter');

module.exports = class Handler{
  constructor(req, res){
    this.req = req;
    this.res = res;

    this.emitter = ee({});
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    this.emitter.on('ok', resp =>{
      clearTimeout(this.z);
      res.send(resp).end();
    });
    this.emitter.on('error', (message, code) => {
      clearTimeout(this.z);
      res.statusCode = code || 200;
      res.send(message || 'Internal error').end();
    });
    this.z = setTimeout(() =>{
      this.emitter.emit('error', 'timeout error', 504);
    }, 3000);

  }

  getResponse(){
    const method = this.req.method.toLowerCase();

    if(typeof this['handle_'+method] === 'function'){
      this['handle_'+method]();
    }else{
      this.emitter.emit('error', 'not found', 400);
    }
  }

  checkCache(){
    let cacheKey = this.getCacheKey();
    this.req.rclient.get(cacheKey, (err, res) => {
      if(err || !res){
        console.info('cache empty');
        this.emitter.emit('dataquery');
        return;
      }
      console.info('response from cache');
      this.req.rclient.quit();
      this.emitter.emit('dataready', JSON.parse(res));
    });
  }

  saveCache(data){
    let cacheKey = this.getCacheKey();
    this.req.rclient.set(cacheKey, JSON.stringify(data), (err, res)=>{
      this.req.rclient.expire(cacheKey, 100, ()=>{
        this.req.rclient.quit();
      });
    });
  }

  sendResponse(data){
    clearTimeout(this.z);
    this.emitter.emit('ok', data);
  }

  handle_options(){
    this.emitter.emit('ok', 'GET,POST,PUT,OPTIONS');
  }

  handle_get(){
    this.emitter.on('dataquery', () => this.googleRequest());
    this.emitter.on('dataready', data => this.sendResponse(data));
    this.checkCache();
  }

  handle_post(){
    this.emitter.emit('error', 'response post method not implemented', 501);
  }

  handle_put(){
    this.emitter.emit('error', 'response put method not implemented', 501);
  }
};
