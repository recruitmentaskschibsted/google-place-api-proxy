'use strict';
const fs = require('fs'),
    http = require('http'),
    express = require('express'),
    app = express(),
    ListHandler = require('./lib/ListHandler.js'),
    DetailHandler = require('./lib/DetailHandler.js'),
    RedisClient = require('./middleware/RedisClient.js').RedisClient,
    GoogleClient = require('./middleware/GoogleClient.js').GoogleClient;


app.use(RedisClient());
app.use(GoogleClient());

app.get('/places', function (req, res) {
 var respHandler = new ListHandler(req, res);
  respHandler.getResponse(req, res);
});
app.get('/places/:id', function (req, res) {
 var respHandler = new DetailHandler(req, res);
  respHandler.getResponse(req, res);
});


const httpServer = http.createServer(app);

httpServer.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
