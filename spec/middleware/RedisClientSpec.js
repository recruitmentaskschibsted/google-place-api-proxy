const rewire = require('rewire');
describe('RedisClient', ()=>{
  let RedisClientMock, redisClient, redisConnectorMock;
  beforeEach(()=>{
    redisConnectorMock = jasmine.createSpyObj('redis', ['createClient']);
    redisConnectorMock.createClient.and.returnValue({type: 'redisclient'})
    RedisClientMock = rewire('../../middleware/RedisClient.js');
    RedisClientMock.__set__('redis', redisConnectorMock);

  });
  it(' should implemented middleawre interface', ()=>{
      redisClient = RedisClientMock.RedisClient();
      expect(redisConnectorMock.createClient).not.toHaveBeenCalled();
      expect(typeof redisClient).toEqual('function');
      let ob1 = {}, ob2 = {}, fn = jasmine.createSpy('next');
      redisClient(ob1, ob2, fn);
      expect(redisConnectorMock.createClient).toHaveBeenCalled();
      expect(ob1.rclient).toBeDefined();
      expect(ob1.rclient).toEqual({type: 'redisclient'});
      expect(fn).toHaveBeenCalled();
  });
})
