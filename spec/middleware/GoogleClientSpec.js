const rewire = require('rewire');

describe('GoogleClient', ()=>{
  let GoogleClientMock, googleConnectorMock, googleClient;
  beforeEach(()=>{
    googleConnectorMock = jasmine.createSpyObj('gclient', ['createClient']);
    GoogleClientMock = rewire('../../middleware/GoogleClient.js');
    GoogleClientMock.__set__('googleMapsClient', googleConnectorMock);

  });
  it(' should implemented middleawre interface', ()=>{
      googleClient = GoogleClientMock.GoogleClient();

      expect(typeof googleClient).toEqual('function');
      let ob1 = {}, ob2 = {}, fn = jasmine.createSpy('next');
      googleClient(ob1, ob2, fn);

      expect(ob1.gclient).toBeDefined();
      expect(ob1.gclient).toEqual(googleConnectorMock);
      expect(fn).toHaveBeenCalled();
  });
})
