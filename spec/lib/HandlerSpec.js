const rewire = require('rewire'),
  Handler = require('../../lib/Handler.js'),
  ee = require('event-emitter');
  let emitterMock;

describe('Handler ',  () => {
    let handler, res, req;
    beforeEach(()=>{
      spyOn(console, 'log');
      spyOn(console, 'info');
      spyOn(console, 'error');
      res = jasmine.createSpyObj('res', ['send', 'header', 'end']);
      req = jasmine.createSpyObj('req', ['method', 'rclient']);
      res.send.and.returnValue(res);
      handler = new Handler(req, res);

    });
    it('should have defined a couple of methods', ()=>{
      expect(handler.getResponse).toBeDefined();
      expect(handler.checkCache).toBeDefined();
      expect(handler.sendResponse).toBeDefined();
      expect(handler.saveCache).toBeDefined();
      expect(handler.handle_options).toBeDefined();
      expect(handler.handle_get).toBeDefined();
      expect(handler.handle_post).toBeDefined();
      expect(handler.handle_put).toBeDefined();
    });
    it('constructor should defined some properties', ()=>{
        expect(handler.req).toEqual(req);
        expect(handler.res).toEqual(res);
        const emiter = ee({})
        expect(handler.emitter).toEqual(emiter);
        expect(res.header).toHaveBeenCalled();
    });
});
describe('Handler with rewired mocks', ()=>{
  let handler, handlerMock, res, req;
  beforeEach(()=>{
      spyOn(console, 'log');
      spyOn(console, 'info');
      spyOn(console, 'error');
      handlerMock = rewire('../../lib/Handler.js');
      res = jasmine.createSpyObj('res', ['send', 'header', 'end']);
      req = jasmine.createSpyObj('req', ['method', 'rclient']);
      res.send.and.returnValue(res);
      emitterMock = {
        _eventArray: [],
        on: function(eventName, cb){
          let found = false;
          this._eventArray.map(elem=>{
            if(elem.name === eventName){
              found = true;
              elem.callbacks.push(cb);
            }
          });
          if(!found){
            this._eventArray.push({name: eventName, callbacks: [cb]})
          }
        },
        emit: function(){
            var eventName = Array.prototype.splice.call(arguments,0,1).join();
            this._eventArray.map(elem=>{
              if(elem.name === eventName){
                elem.callbacks.map(fn => {
                  fn(...arguments);
                });
              }
            });
        }
      }
      spyOn(emitterMock, 'on').and.callThrough();
      spyOn(emitterMock, 'emit').and.callThrough();
      handlerMock.__set__('ee', ()=>emitterMock);
  })
  it('after init emitter should starts to listen smth', ()=>{
      handler = new handlerMock(req, res);
      expect(emitterMock.on).toHaveBeenCalled();
      expect(emitterMock.on.calls.count()).toEqual(2);
  })
  it('getResponse should call further proper method', ()=>{
      req.method = 'get';
      spyOn(handlerMock.prototype, 'handle_get');
      handler = new handlerMock(req, res);
      handler.getResponse();
      expect(handler.handle_get).toHaveBeenCalled();
      req.method = 'options';
      spyOn(handlerMock.prototype, 'handle_options');
      handler.getResponse();
      expect(handler.handle_options).toHaveBeenCalled();
      req.method = 'post';
      spyOn(handlerMock.prototype, 'handle_post');
      handler.getResponse();
      expect(handler.handle_post).toHaveBeenCalled();
      req.method = 'put';
      spyOn(handlerMock.prototype, 'handle_put');
      handler.getResponse();
      expect(handler.handle_put).toHaveBeenCalled();
      req.method = 'patch';

      handler.getResponse();
      expect(emitterMock.emit).toHaveBeenCalled();
      expect(emitterMock.emit).toHaveBeenCalledWith('error', 'not found', 400);
  });
  it('call get method should involved proper flow', ()=>{
      req.method = 'get';
      req.rclient = jasmine.createSpyObj('redis', ['get', 'quit']);
      req.rclient.get.and.callFake(function(ck, cb){
        cb(null, "{\"data\": \"test\"}")
      });

      handlerMock.prototype.getCacheKey = function(){
        return 'testcachestrng';
      };
      spyOn(handlerMock.prototype, 'getCacheKey').and.callThrough();
      handler = new handlerMock(req, res);
      spyOn(handler, 'sendResponse').and.callThrough();
      handler.getResponse();
      expect(handlerMock.prototype.getCacheKey).toHaveBeenCalled();
      expect(emitterMock.emit).toHaveBeenCalledWith('dataready', {data: "test"});
      expect(handler.sendResponse).toHaveBeenCalledWith({data: "test"});
      expect(emitterMock.emit).toHaveBeenCalledWith('ok', {data: "test"});
      expect(res.send).toHaveBeenCalledWith({data: "test"})
      expect(res.end).toHaveBeenCalled();
      expect(req.rclient.quit).toHaveBeenCalled();
  });
  it('options should emit and return a string', ()=>{
      req.method = 'options';
      handler = new handlerMock(req, res);
      handler.getResponse();
      expect(emitterMock.emit).toHaveBeenCalledWith('ok', 'GET,POST,PUT,OPTIONS');
      expect(res.send).toHaveBeenCalledWith('GET,POST,PUT,OPTIONS');
      expect(res.end).toHaveBeenCalled();
  });
  it('not implemeted methods should emit an error', ()=>{
      req.method = 'post';
      handler = new handlerMock(req, res);
      handler.getResponse();
      expect(emitterMock.emit).toHaveBeenCalledWith('error', 'response post method not implemented', 501);
      expect(res.statusCode).toEqual(501);
      expect(res.send).toHaveBeenCalledWith('response post method not implemented');
      req.method = 'put';
      res.statusCode = 200;
      handler.getResponse();
      expect(emitterMock.emit).toHaveBeenCalledWith('error', 'response put method not implemented', 501);
      expect(res.statusCode).toEqual(501);
      expect(res.send).toHaveBeenCalledWith('response put method not implemented');
  });
  it('should send timeout after 3 sec.', (done)=>{
    req.method = 'get';

    handler = new handlerMock(req, res);

    spyOn(handler, 'handle_get');
    handler.getResponse();
    setTimeout(function() {
      expect(emitterMock.emit).toHaveBeenCalledWith('error', 'timeout error', 504);
      expect(res.send).toHaveBeenCalledWith('timeout error');
      done();
    }, 3005);

  });
});
