const DetailHandler = require('../../lib/DetailHandler.js'),
  Handler = require('../../lib/Handler.js');

  describe('DetailHandler',  () => {
    let handler, emitterMock, res, req;
    beforeEach(()=>{
      spyOn(console, 'log');
      spyOn(console, 'info');
      spyOn(console, 'error');
      res = jasmine.createSpyObj('res', ['send', 'header', 'end']);
      req = jasmine.createSpyObj('req', ['method', 'rclient']);
      res.send.and.returnValue(res);
      req.method = 'get';
      req.params = {id: 'kooltest'};
      req.rclient = jasmine.createSpyObj('rclient', ['get']);
      req.gclient = jasmine.createSpyObj('glient', ['place']);

      handler = new DetailHandler(req, res);
      spyOn(handler.emitter, 'emit').and.callThrough();
    });

    it('some methods should be defined', ()=>{
        expect(handler.googleRequest).toBeDefined();
        expect(handler.getCacheKey).toBeDefined();
        expect(handler instanceof Handler).toBe(true);
    });

    it('getCacheKey method should be always called', ()=>{

      spyOn(handler,'checkCache').and.callThrough();
      spyOn(handler,'getCacheKey').and.callThrough();
      handler.getResponse();
      expect(handler.getCacheKey).toHaveBeenCalled();
      expect(handler.checkCache).toHaveBeenCalled();
      expect(handler.getCacheKey()).toEqual('detail-kooltest');
    });

    it('googleRequest method should be called and work well', ()=>{

      req.rclient.get.and.callFake(function(ck, cb){
        cb(null, null);
      });
      req.gclient.place.and.callFake(function(params, cb){
        cb(null, {json: {data: "place query data test"}});
      });
      spyOn(handler,'googleRequest').and.callThrough();
      spyOn(handler, 'saveCache');

      handler.getResponse();
      expect(handler.googleRequest).toHaveBeenCalled();
      expect(req.gclient.place).toHaveBeenCalled();
      expect(req.gclient.place.calls.mostRecent().args[0].language).toEqual('pl');
      expect(req.gclient.place.calls.mostRecent().args[0].placeid).toEqual('kooltest');

      expect(handler.saveCache).toHaveBeenCalledWith({data: 'place query data test'});
      expect(handler.emitter.emit).toHaveBeenCalledWith('dataready', {data: 'place query data test'});
    });

    it('negative scenario: an error should be emited', ()=>{

      req.rclient.get.and.callFake(function(ck, cb){
        cb(null, null);
      });
      req.gclient.place.and.callFake(function(params, cb){
        cb({error: {code: 666, message: "bugs again!"}});
      });
      spyOn(handler, 'saveCache');

      handler.getResponse();

      expect(req.gclient.place).toHaveBeenCalled();
      expect(handler.saveCache).not.toHaveBeenCalled();
      expect(handler.emitter.emit).toHaveBeenCalledWith('error', {error: {code: 666, message: "bugs again!"}}, 501);
    });
  });
