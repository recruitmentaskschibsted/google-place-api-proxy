const ListHandler = require('../../lib/ListHandler.js'),
  Handler = require('../../lib/Handler.js');

describe('ListHandler',  () => {
  let handler, handlerMock, emitterMock, res, req;
  beforeEach(()=>{
    /* silence */
    spyOn(console, 'log');
    spyOn(console, 'info');
    spyOn(console, 'error');

    res = jasmine.createSpyObj('res', ['send', 'header', 'end']);
    req = jasmine.createSpyObj('req', ['method', 'rclient']);
    res.send.and.returnValue(res);
    req.method = 'get';
    req.query = {filter: 'testfilter', pagetoken: 'testpagetoken'};
    req.rclient = jasmine.createSpyObj('rclient', ['get', 'quit']);
    req.gclient = jasmine.createSpyObj('glient', ['placesNearby']);

    handler = new ListHandler(req, res);
    spyOn(handler.emitter, 'emit').and.callThrough();
  });

  it('some methods should be defined', ()=>{
      expect(handler.googleRequest).toBeDefined();
      expect(handler.getCacheKey).toBeDefined();
      expect(handler instanceof Handler).toBe(true);
  });

  it('getCacheKey method should be always called', ()=>{

    spyOn(handler,'checkCache').and.callThrough();
    spyOn(handler,'getCacheKey');
    handler.getResponse();
    expect(handler.getCacheKey).toHaveBeenCalled();
    expect(handler.checkCache).toHaveBeenCalled();
  });

  it('googleRequest method should be called and work well', ()=>{

    spyOn(handler,'checkCache').and.callThrough();
    spyOn(handler,'getCacheKey').and.returnValue('testchachekey');
    req.rclient.get.and.callFake(function(ck, cb){
      cb(null, null);
    });
    req.gclient.placesNearby.and.callFake(function(params, cb){
      cb(null, {json: {data: "google query data test"}});
    });
    spyOn(handler,'googleRequest').and.callThrough();
    spyOn(handler, 'saveCache');

    handler.getResponse();
    expect(handler.googleRequest).toHaveBeenCalled();
    expect(req.gclient.placesNearby).toHaveBeenCalled();
    expect(req.gclient.placesNearby.calls.mostRecent().args[0].language).toEqual('pl');
    expect(req.gclient.placesNearby.calls.mostRecent().args[0].name).toEqual('testfilter');
    expect(req.gclient.placesNearby.calls.mostRecent().args[0].pagetoken).toEqual('testpagetoken');
    expect(handler.saveCache).toHaveBeenCalledWith({data: 'google query data test'});
    expect(handler.emitter.emit).toHaveBeenCalledWith('dataready', {data: 'google query data test'});
  });

  it('negative scenario: an error should be emited', ()=>{
    spyOn(handler, 'checkCache').and.callThrough();
    spyOn(handler, 'getCacheKey').and.returnValue('testchachekey');
    req.rclient.get.and.callFake(function(ck, cb){
      cb(null, null);
    });
    req.gclient.placesNearby.and.callFake(function(params, cb){
      cb({error: {code: 666, message: "bugs again!"}});
    });
    spyOn(handler,'googleRequest').and.callThrough();
    spyOn(handler, 'saveCache');
    handler.getResponse();
    expect(handler.googleRequest).toHaveBeenCalled();
    expect(req.gclient.placesNearby).toHaveBeenCalled();
    expect(handler.saveCache).not.toHaveBeenCalled();
    expect(handler.emitter.emit).toHaveBeenCalledWith('error', {error: {code: 666, message: "bugs again!"}}, 501);
  });
});
