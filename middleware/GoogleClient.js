let googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyCzlFXwaR6Ka-KhNVtiRy88_hMSKEGuKe0'
});

module.exports = {
  GoogleClient: function(){
    return function(req, res, next){
      req.gclient = googleMapsClient;
      next();
    }
  }
}
